﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public float maxSpeed = 5.0f;

    void Update()
    {
        // get the input values
        Vector2 direction1;
          direction1.x = Input.GetAxis("HorizontalP2");
            direction1.y = Input.GetAxis("VerticalP2");

        // scale by the maxSpeed parameter
        Vector2 velocity = direction1 * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}
